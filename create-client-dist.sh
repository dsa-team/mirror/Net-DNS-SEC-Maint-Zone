#!/bin/bash
# $Id: create-client-dist.sh,v 1.6 2004/07/02 15:29:03 olaf Exp $
#
#
# Use this script to create a package for the dnssigner_client distribution
# The resulting tarball will be called something like:
#  Net-DNS-SEC-Maint-ZoneSigner-<version>.tar.gz
# It has less dependencies than the the whole package
#

rm -rf tmp-client-dist; mkdir tmp-client-dist

cp ZoneSigner.pm tmp-client-dist
cp apps/dnssigner_client tmp-client-dist

cat > tmp-client-dist/Makefile.PL <<EOF
use ExtUtils::MakeMaker;

WriteMakefile(
              'NAME'    => 'Net::DNS::SEC::Maint::ZoneSigner',
              'VERSION_FROM' => 'ZoneSigner.pm', # finds $VERSION
              'PREREQ_PM'             => {
                File::Basename => 0.0,
                Getopt::Std    => 0.0,
                IO::Handle => 0.0,
                SOAP::Lite => 0.0,
              },

              'EXE_FILES'  => [ "dnssigner_client"  ],


              );
EOF

cat > tmp-client-dist/MANIFEST <<EOF
Makefile.PL
MANIFEST
dnssigner_client
ZoneSigner.pm
test.pl
EOF

cat > tmp-client-dist/test.pl <<EOF
BEGIN { print "1..1\n" }
use Net::DNS::SEC::Maint::ZoneSigner;
print "ok 1\n";
EOF

cd tmp-client-dist
perl Makefile.PL
make dist
mv *.tar.gz ..
cd ..
rm -rf tmp-client-dist
