package Net::DNS::SEC::Maint::ZoneSignerService;

use SOAP::Transport::HTTP;
use strict;

sub Log {
  # TODO: Logging
  # print STDERR @_
}

sub new {
  my $class = shift;
  return bless {@_}, $class;
}

sub setLocalAddr { $_[0]->{LocalAddr} = $_[1] }

sub getLocalAddr { $_[0]->{LocalAddr} || 'localhost' }

sub setLocalPort { $_[0]->{LocalPort} = $_[1] }

sub getLocalPort { $_[0]->{LocalPort} || 8001 }

sub run {
  my $self = shift;

  my $daemon = SOAP::Transport::HTTP::Daemon->new
    (
     LocalAddr => $self->getLocalAddr(),
     LocalPort => $self->getLocalPort(),
     ReuseAddr => 1,

    );



  $daemon->objects_by_reference('Net::DNS::SEC::Maint::Zone::Wrapper');
  $daemon->dispatch_to('Net::DNS::SEC::Maint::Zone::Wrapper');

  Log "Contact to SOAP server at ", $daemon->url, "\n";

  $daemon->handle;
}

package Net::DNS::SEC::Maint::Zone::Wrapper;

use strict;

use Net::DNS::SEC::Maint::Zone;

require File::Temp;

sub new {
  my $class = shift;
  return bless {@_}, $class;
}

sub signZone {
  my $self = shift;
  my %args = @_;

  my $zone = $args{zone};
  my $origin = $args{origin};
  my $start = $args{start};
  my $end = $args{end};

  my ($zfh, $zonefile) = File::Temp::tempfile( DIR => '/tmp' );
  my $oldhandle=select $zfh;
  $|=1;
  select $oldhandle;

  print $zfh $zone;

  my $signedzone;

  eval {
    my $z = Net::DNS::SEC::Maint::Zone->read(0,$zonefile, $origin);

    $z->set_sig_start($start) if $start ;
    $z->set_sig_end($end) if $end ;
    $z->sign;

    $signedzone = $z->get_signedzone || die join '', $z->get_sign_errors;

    # TODO: Do someting with system messages
    # my @sys_msg = $zone->get_sign_system;
  };
  my $e = $@;

  $zfh->close;
  unlink $zonefile;

  die $e if $e;

  return $signedzone;
}

sub DESTROY {
  my $self = shift;
  undef $self;
}

sub testEcho {
  my $self = shift;

  return @_;
}

sub testError {
  my $self = shift;

  die @_;
}

sub testPersist {
  my $self = shift;

  return ++$self->{i};
}

sub testExit { exit }

1;

__END__

=head1 NAME

Net::DNS::SEC::Maint::ZoneSignerService - Zone Signer Server Module

=head1 SYNOPSIS

  use Net::DNS::SEC::Maint::ZoneSignerService;

  $s = Net::DNS::SEC::Maint::ZoneSignerService->new;

  $s->run;

=head1 DESCRIPTION

This Perl module is a wrapper for Net::DNS::SEC::Maint::Zone to make
it avaliable over network connections and serve
Net::DNS::SEC::Maint::ZoneSigner which can be used to signe DNS zones.

=head1 METHODS

=over 1

=item new ( )

Constructor.

=item setLocalAddr ( ADDR )

Sets the local address to accept connections from.

=item getLocalAddr ( )

Returns the local address which connections are expected
from. Defaults to 'localhost'

=item setLocalPort ( PORT )

Sets the TCP port to accept connections from.

=item getLocalPort ( )

Returns the TCP port which connections are expected
from. Defaults to '8001'

=item run ( )

Starts listening for connections on given address and port.

=back

=head1 AUTHOR

Ziya Suzen E<lt>ziya@ripe.netE<gt>

=head1 SEE ALSO

perl(1), Net::DNS::SEC::Maint::ZoneSigner(3),
Net::DNS::SEC::Maint::Zone(3).

=head1 COPYRIGHT

Copyright (c) 2003                                          RIPE NCC 

All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the author not be
used in advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS; IN NO EVENT SHALL
AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

