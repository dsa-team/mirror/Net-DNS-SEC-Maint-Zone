# $Id: Zone.pm,v 0.50 2005/07/19 10:05:08 olaf Exp $
#
#

=head1 NAME

Net::DNS::SEC::Maint::Zone - Zone object for use in the Net::DNS::SEC::Maint framework

=head1 SYNOPSIS

  use Net::DNS::SEC::Maint::Zone;


=head1 DESCRIPTION

The Net::DNS::SEC::Maint::Zone object essentially contains one main
attribute, a textual representation of a zone.

The class can be used to pass a (signed) zone back and forth from a
client to a (signing) server.


The class uses the 'tmpdir' configuration as configured in for
Net::DNS::SEC::Maint::Key.

=cut

package Net::DNS::SEC::Maint::Zone;


use Net::DNS::Zone::Parser;
use strict;
use Carp;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK $AUTOLOAD);
use Fcntl qw/F_SETFD F_GETFD/;
use POSIX qw(tmpnam);
use POSIX qw(:signal_h :sys_wait_h);

#require Exporter;
#require DynaLoader;
#require AutoLoader;

BEGIN{ 
    
    $Net::DNS::Zone::Parser::REVISION >465 || die "Sorry an essentiall library is not in sync\n".
	"Use subversion to get the latest parser from \n".
	"www.net-dns.org/svn/net-dns-zone-parser/trunk/";
}


@ISA = qw( );
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
);


$VERSION = do { my @r=(q$Revision: 0.50 $=~/\d+/g); sprintf "%d."."%03d"x$#r,@r };

my $cleanup=1; # Cleanup temporary files (containing zone and signed zones. 
               # should to 1 by default.


my $debug=0;



#use File::Temp qw( tempfile tempdir );
use File::Basename;
use Data::Dumper;
use IO::File;


my $_DNSSEC_KEY_MODULE_LOADED;

BEGIN {
        eval { require Net::DNS::SEC::Maint::Key};
        # $@ will be true if any errors where encountered 
        # loading SIG.pm
        $_DNSSEC_KEY_MODULE_LOADED = $@ ? 0 : 1;

}



#
#  Helper function. We mainain a counter class attribute  (for random files)
#  get_count will return a number and increase the counter


BEGIN {
    my $_tmpcounter=0;
    sub get_count {
	return ++$_tmpcounter;
    }
}





my $signzone_dtd="
<!-- The signzone either contains call (contains the information that -->
<!-- is being from the client to the server) or return (contains the  -->
<!-- information that goes from the server to client.)                -->

<!ELEMENT signzone (call|return)>
<!-- The version of the form                                          -->
<!ATTLIST signzone                      version CDATA #FIXED \"0.1\">

<!-- call content                                                     -->
<!ELEMENT call  (options?,zone)>
<!ATTLIST call  origin CDATA #REQUIRED>
<!ELEMENT options  (sig_start?,sig_end?) >
<!ELEMENT sig_start (#PCDATA) >
<!ELEMENT sig_end (#PCDATA) >
<!ELEMENT zone  (#PCDATA) >

<!-- return content                                                   -->
<!ELEMENT return ( error*, system*, signedzone? , signzonecommand )>
<!ATTLIST return  origin CDATA #REQUIRED>
<!ELEMENT signedzone  (#PCDATA) >\
<!ELEMENT signzonecommand  (#PCDATA) >
<!ELEMENT error  (#PCDATA) >
<!ELEMENT system  (#PCDATA) >
<!ATTLIST system  verbose  (yes|no)   #IMPLIED>

";

=head1 METHODS

=head2 new

Creates a new zone object.

Optionally a path to a directory in which temporary files can be
stored can be passed as argument.


=cut



sub new {
    my $caller=shift;

    my $self={};
    my $conftmpdir=shift;


    my $class=ref($caller)||$caller;

    bless $self,$class;
    $self->_set_defaults;

    my $keydb;


    if ($conftmpdir) {
	$conftmpdir =~ s/\s*$//;
	$conftmpdir =~ s/^\s*//;
	die "conftmpdir contains spaces... $conftmpdir" if $conftmpdir =~/\s/;
	$conftmpdir .= "/" if $conftmpdir !~ /\/$/;  # add trailing dash if not present
    }else{
	if ($_DNSSEC_KEY_MODULE_LOADED){
	    $keydb=Net::DNS::SEC::Maint::Key->new(1);
	    $conftmpdir=$keydb->getconf("tmpdir");
	    $conftmpdir =~ s/\s*$//;
	    $conftmpdir =~ s/^\s*//;
	    die "conftmpdir contains spaces... $conftmpdir" if $conftmpdir =~/\s/;
	    $conftmpdir .= "/" if $conftmpdir !~ /\/$/;  # add trailing dash if not present
	    
	} else {
	    print "Warning: Using /tmp as place to store tmpfiles\n";
	    $conftmpdir="/tmp";

	}
    }
    

    die "$conftmpdir does not exist" unless -d $conftmpdir ;

    my $ORIGIN=$self->get_origin;
    my $tempdir = "$conftmpdir";



    $self->{'tempdir'}=$tempdir;

    my ($tmpzone_fh,$tmpzone_name) = tempfile($tempdir);

    $self->{'tmpzone_fh'}=$tmpzone_fh;
    $self->{'tmpzone_name'}=$tmpzone_name;



    my ($never_used_fh,$tmpsigned_name) = tempfile($tempdir  );


    $self->{'signed_name'}="";
    $self->{'tmpsigned_name'}=$tmpsigned_name;

    my $oldhandle=select $self->{'tmpzone_fh'};
    $|=1;
    select $oldhandle;

    return $self;
}






=head2 read


    my $zone=Net::DNS::SEC::Maint::Zone->read(0,"/tmp/example.foo");
    my $zone=Net::DNS::SEC::Maint::Zone->read(1,"/tmp/foo.db","example.db");

'read' is a constructor method that reads a 'bind-format' zonefile
from disk.  It takes 2 or 3 arguments. The second argument is a path to
the zonefile. The third paramater is the 'origin' of the zone. If the
second parameter is ommited the 'origin' is addumend to be the same as
the name of the file.


If the first arguments evaluates to true, the SOA serial number will 
be increase at the time the zone is parsed.

During parsing the DNSKEY RRs and their RRSIG signatures are removed. This
way the zone's  keyset will remain consistent with the data in the
database. All other signatures, in the zone and the apex are read as
is. All other RRSIGs will be retained.

The $INCLUDE directive can be used to "slurp" in other files. I am not
convinced if the semantics are the same as for BIND.

$INCLUDE <path> 

will read the file as specified by 'path'. If 'path' is absolute it
will be interpreted as such. If it is relative it will be taken
relative to the path of the zonefile that includes it. 

Optionally $INCLUDE will take a 2nd argument that sets the current
origin for relative domains. 


The parser only accept IN class zone files.

=cut
#'

sub read {
    my $class=shift;
    my $bump_soa=shift;
    my $possible_filename=shift;
    my $origin=shift;

    my $self=$class->new;
    my $fh=$self->{"tmpzone_fh"};

    my $parser=Net::DNS::Zone::Parser->new($fh);
    my $returnval= $parser->read($possible_filename,{
				      ORIGIN=>$origin,
				      STRIP_DNSKEY=>1,   #STRIP DNSKEYs
				      BUMP_SOA=> $bump_soa,
				     });

    die "$returnval" if $returnval;
    $self->{"origin"}=$parser->get_origin;
    return $self;
}







=head2 get_sig_start, get_sig_end, set_sig_start, set_sig_end.

Accessor methods to the signature inceptation and signature expiration
time that are passed to the signer. If the vallues are not set the
signers defaults will be used.  The format of these vallues are as they should
be passed to dnssec-signzones -s and -e arguments.


Returns 'undef' if the value is not defined.

=cut


# TODO some more sanity checks on the input values.

sub get_sig_start {
    my $self=shift;
    return $self->{'sig_start'};
}    


sub set_sig_start {
    my $self=shift;
    my $value=shift;
    # dnssec-signzone:
    # 	-s YYYYMMDDHHMMSS|+offset:
    #	SIG start time - absolute|offset (now)
    if ( $value !~ /^\d{14}/   &&   # matches YYYYMMDDHHMMSS format
	 $value !~ /\+\d+/       # matches +offset
	)
    {
	croak "Not a valid format for start time: $value " ;
    }
    return $self->{'sig_start'}=$value;
}    

sub get_sig_end {
    my $self=shift;
    return $self->{'sig_end'};
}    


sub set_sig_end {
    my $self=shift;
    my $value=shift;
    # dnssec-signzone
    #	-e YYYYMMDDHHMMSS|+offset|"now"+offset]:
    #		SIG end time  - absolute|from start|from now (now + 30 days)
    
    if ( $value !~ /^\d{14}/   &&   # matches YYYYMMDDHHMMSS format
	 $value !~ /^(now)?\+\d+/       # matches +offset and now+offset
	 )
    {
	croak "Not a valid format for start time: $value " ;
    }    
    return $self->{'sig_end'}=$value;
}

# Initialize attributes...  Will only set attributes if not
#defined... so it can be called at the end of a constructor method.

sub _set_defaults {
    my $self=shift;
    $self->{'signed_zone'}="" if !defined $self->{'signed_zone'};
#    $self->{'zone'}="" if !defined $self->{'zone'};
    $self->{'origin'}="" if !defined $self->{'origin'};
    $self->{'sign_err'}=[] if !defined $self->{'sign_err'} ;
    $self->{'sign_system'}=[]if !defined  $self->{'sign_system'};    
    $self->{'signzonecommand'}="" if !defined  $self->{'signzonecommand'};
    $self->{'signzone_dtd'}=$signzone_dtd;
}



=head2 get_signzonescommand 

    print "Zone signed with: ".$zone->get_signzonecommand ."\n";


Returns the command used for signing with all the parameters and
temporary files. The command is passed back to the client so that the 
client can see which command has been executed on the server.

=cut

sub get_signzonecommand {
    my $self=shift;
    return $self->{'signzonecommand'};
}    


=head2 sign

    $zone->sign;
    $zone->sign($ttl);

Uses the Net::DNS::SEC::Maint::Key database to sign the zone.
Returns 0 on succes and an errorstring on failure.


The optional argument specifies the TTL of the APEX keyset. If not
defined the default ttl as specified by the $TTL statement in the zone
will be used. If that is not known or set to 0 a value of 3600 will be
used.

=cut

sub sign {
    my $self=shift;
    my $ttl_on_key=shift;

    # Do not allow changes to the output name...
    $self->{"singname_may_not_be_set"}=1;
    # Only if the key module is available.
    if (! $_DNSSEC_KEY_MODULE_LOADED ){
	print "Singing functionality is not available because Net::DNS::SEC::Maint::Key is not found in the library path.\n";
	return 0;
    }


    my $keydb=Net::DNS::SEC::Maint::Key->new(1);

    my $ORIGIN=$self->get_origin;

    return "No ORIGIN in the zone file " if !$ORIGIN;



    $ttl_on_key=$self->get_default_ttl if ! $ttl_on_key ;
    $ttl_on_key=3600 if ! $ttl_on_key ;


    my $keyset=$keydb->get_keyset($ORIGIN,$ttl_on_key);

    

    my $fh=$self->{'tmpzone_fh'};
    print "KEYSET:".$keyset."\n" if $debug;
    print "Warning Empty Keyset\n" unless $keyset;
    print $fh  "\n". $keyset."\n";

    my @keysigningkeys=$keydb->get_active_key($ORIGIN);
    my @zonesigningkeys=$keydb->get_active_zone($ORIGIN);


    if (!@zonesigningkeys){
	return "There are no active zone keys for $ORIGIN\n";
    }
    my $nsec3_zone = $zonesigningkeys[0]->is_algorithm_nsec3;
       


    $self->{'signer_key_signing_keys'}=[];
    foreach my $key (@keysigningkeys){
	push @{$self->{'signer_key_signing_keys'}},$key->get_keypath;

    }
    $self->{'signer_zone_signing_keys'}=[];
    foreach my $key (@zonesigningkeys){
	push @{$self->{'signer_zone_signing_keys'}},$key->get_keypath;
    }
	

#    print "\n";
#    print ";;-----------------sonesinging keys ------------\n; ";
#    print join "\n; ", @zonesigningkeys;
#    print "\n";
    my $signzonecommand=$keydb->getconf("dnssec_signzone");
    my $tmpsignzonecommand=$signzonecommand;
    $tmpsignzonecommand=~ s/^(\S*)\s*.*/$1/;    #strip flags
    


    die $tmpsignzonecommand
      . " is not executable\n "
      . "We will not be able to create keys\n "
      . "make sure that the proper value for dnssec_signzone is specified in\n "
      . $keydb->getconf("conffile")
      . "\n or set the environment variable DNSSECMAINT_DNSSEC_SIGNZONE \n"
      if !-x $tmpsignzonecommand;



    $signzonecommand .= " -s ".$self->get_sig_start . " " if 
	$self->get_sig_start;
    $signzonecommand .= " -e ".$self->get_sig_end . " " if 
	$self->get_sig_end;



    
    my $signfilename=$self->signfilename;

    $signzonecommand .= " -t -f ". $signfilename. "  -o " . $ORIGIN ;
    if ($nsec3_zone) {
	open(RND, '<', "/dev/urandom") || die ("Cannot open /dev/random to get an nsec3 seed\n");
	my $rnd;
	my $READ = 8;
	my $res = sysread(RND, $rnd, $READ);
	if (!defined($res) || length($rnd) != $READ) {
		die ("Could not read $READ bytes of randomness");
	};
	my $seed = unpack("H*", $rnd);
	$signzonecommand .= " -3 $seed";
    }
    $signzonecommand .=	" -k  ". join(" -k ", @{$self->{'signer_key_signing_keys'}}) if @keysigningkeys;
    $signzonecommand .=   " ".$self->{'tmpzone_name'}. " ". join("  ", @{$self->{'signer_zone_signing_keys'}});

    $self->{'signzonecommand'}=$signzonecommand;
    print ";;SIGNZONECOMMAND: ".$signzonecommand."\n" if  $debug>1 ;;

    use IPC::Open3;
    use IO::Select;    

    $SIG{'PIPE'} = sub {
        	return "SIGPIPE received while running $signzonecommand";
    };

    my $pid = open3( \*CMD_IN, \*CMD_OUT, \*CMD_ERR, $signzonecommand );
    $SIG{CHLD}= sub{ 
	if(waitpid($pid,0)>0){
	    my $signalnum= $? & 127;
	    die "$signzonecommand \n did not exit normally (signal $signalnum)" if ! WIFEXITED($?);
	    
	}
    };

    close(CMD_IN);

    # For some reason the parent will need to sleep a bit
    # otherwise I wont catch the STDERR

    sleep (1);

    my $selector = IO::Select->new();
    $selector->add( \*CMD_ERR, \*CMD_OUT );
    my @ready;
    while ( @ready = $selector->can_read ) {
        my $fh;
        foreach $fh (@ready) {
            if ( fileno($fh) == fileno(\*CMD_OUT) ) {
		push @{$self->{'sign_system'}}, scalar <CMD_OUT> ;
            }

            elsif ( fileno($fh) == fileno(\*CMD_ERR) ) {
                push @{$self->{'sign_err'}}, scalar <CMD_ERR> ;
            }

            else {
                return "WEIRD error";
            }

            $selector->remove($fh) if ( eof($fh) );
        }
    }
    
    close(CMD_OUT);
    close(CMD_ERR);

    if ($debug>1) {
	print "--------------------- sign stderr ---------------\n";
	print join ( "\n", $self->get_sign_errors) ;
	print "--------------------- sign stdout  ---------------\n";
	print join ( "\n", $self->get_sign_system) ;
	print "-------------------------------------------------\n";
    }
    my $i=0;
    while ($i< @{$self->{"sign_err"}}){
	last unless defined $self->{"sign_err"}->[$i];
	print  $self->{"sign_err"}->[$i];
	return $self->{"sign_err"}->[$i] if  
	    # Match the failings on stderr
	    $self->{"sign_err"}->[$i]=~/^dnssec-signzone: failed/ 
#	  ||  $self->{"sign_err"}->[$i]=~/^dnssec-signzone: warning/ 
	    ;
	    
	$i++;
    }
    0;
}


=head2 get_origin

    $origin=$zone->get_origin;

=cut


sub get_origin {
    my $self=shift;
    return $self->{'origin'};
}



=head2 get_signedzone

    $signedzone=$zone->get_signedzone;
    print $signedzone;

Returns string with the signed zone, may be an emty string.

    
=cut

sub get_signedzone {
    my $self=shift;
    my $returnval="";
    my $signfilename=$self->signfilename;


    open (RESULT,"< $signfilename")|| return(0);
    
    while(<RESULT>){
       chop;
       $returnval.=$_."\n";
      }
    return $returnval;
}





sub get_zone {
    my $self=shift;
    my $returnval;#="\$ORIGIN ".  $self->get_origin ."\n".
#	"\$TTL ".$self->get_default_ttl."\n";

    my $fh=$self->{"tmpzone_fh"};
    seek($fh,0,0);
    while (<$fh>){
	$returnval.=$_;	
    }
    return $returnval;
}


=head2 get_default_ttl
    $ttl=$zone->get_default_ttl;

Returns the value of the default TTL as set by the '$TTL' statement in
the zonefile. Returns 0 if not known or parsed.

=cut

sub get_default_ttl {
    my $self=shift;
    return $self->{'default_ttl'}|| "0";
}



=head2 get_sign_errors

    @errors=$zone->get_sign_errors;

Returns array with errors that occured during the signing process
(STDERR output)

=cut

sub get_sign_errors {
    my $self=shift;
    my @ret=();
    # There can be "undef"
    foreach my $val ( @{$self->{'sign_err'}} ){
	push @ret, $val if defined $val;
    }
    
    return @ret;
}


=head2 get_sign_system

    @errors=$zone->get_sign_system;

Returns array with system messages from the signing process. (STDOUT output)

=cut


sub get_sign_system {
    my $self=shift;
    my @ret=();
    # There can be "undef"
    foreach my $val ( @{$self->{'sign_system'}}){
	push @ret, $val if defined $val;
    }
    return @ret;
}


=head2 signfilename

$self->signfilename("bla/example.com.signed"); 
my $file=$signfilename();

Instead of using temorary files to store results the output is safed
directly into the specified file. You should not modify the location
after you have signed a zone (after you called the sign method and/or
the read_xml method).  A check is done to make sure this does not
happen and the name is not changed if it leads to confusion.

The function returns the name of the file where the signed data
lives. Note that if this is a temporary file it may not be around.

=cut

sub signfilename {
    my $self=shift;
    if ( @_ ){
	if (defined $self->{"singname_may_not_be_set"}){
	    print "Sorry you have allready changed the name";
	}else{
	    $self->{"signfilename"} = shift;
	    unlink($self->{'tmpsigned_name'})if $cleanup;
	    $self->{"singname_may_not_be_set"}=1;
	    $self->{"singname_has_been_chagned"}=1;
	}
    }
    
    return exists $self->{"signfilename"}
    ? $self->{"signfilename"}
    : $self->{"tmpsigned_name"};
}




#######################
#

# tempfile function creates a new unique tempfile name and returns an
# open IO::File and the name of the file.  The argument is a directory
# in which the file is created.

sub tempfile {
    my $indir=shift;
    # $temp_dir is set to some default:
    my $temp_dir = -d '/tmp' ? '/tmp' : $ENV{TMP} || $ENV{TEMP};
    if (-d $indir ){
	$temp_dir = $indir; 
    }else{
	print "WARNING $indir does not exist using: $temp_dir\n";
    }
    my $fh;
    my $path;
    do {$path=sprintf("%s/MAINTZONE%d-%05d", $temp_dir, time(),get_count);}
      until 	($fh= new IO::File  $path, O_RDWR|O_CREAT|O_EXCL );
    return  ($fh, $path);

}






sub AUTOLOAD { 
    die $AUTOLOAD." has not been implemented\n";
    
}

sub DESTROY {
    my $self=shift;
    
    unlink($self->signfilename) if ! $self->{"singname_has_been_chagned"}
        && $cleanup;
    close($self->{'tmpzone_fh'}) if $cleanup;
    unlink($self->{'tmpzone_name'}) if $cleanup;
    #rmdir($self->{'tempdir'}) if $cleanup;

}



1;


__END__



=head1 Features, bugs and TODO

The class stores both the signed and the unsigned zones in a 'string'
representation in memory. That makes this class less suitable for big
zones on machines with little memory.

BIND accepts TTLs specified with units e.g. 1h30m this is not
supported in this code yet.


=head1 COPYRIGHT

Copyright (c) 2002, 2003  RIPE NCC.  Author Olaf M. Kolkman

All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the author not be used
in advertising or publicity pertaining to distribution of the software
without specific, written prior permission.


THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS; IN NO
EVENT SHALL AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.






=head1 SEE ALSO

L<Net::DNS::SEC::Maint::User>,  L<Net::DNS::SEC::Maint::Key>, dnssec-signzone

=cut









