#!/usr/bin/perl  -sw 
# Test script for Net::DNS::SEC::Maint::Zone
# $Id: 05-soapbox.t,v 1.7 2005/07/26 08:22:30 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-zonetest.t

#
#  Use the keydatabase in the test suite.
$ENV{"DNSSECMAINT_CONFFILE"}="t/dnssecmaint.conf";


use Test::More tests=>8;
use strict;

use POSIX qw(:signal_h :sys_wait_h);



########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################


my $testport=8106;

diag("The testscript contains a variable called \$testport. The server listens on that port");
diag("\$testport currently is set to $testport");
diag("There may be failures binding to a port if a previous testrun failed.");
diag("Change the value of \$testport if that happens");

my $gid=getgrnam "dnssecmt";
if (!defined $gid){
  print "    dnssecmt is an unknown group\n";
  print "    Please add the group to /etc/group or change the maintgroup \n";
  print "    I will try to use your default group.\n";
  $ENV{"DNSSECMAINT_MAINTGROUP"}=getgrgid($();
}

my @possible_signzone= qw(
			./dnssec-signzone 
			~/dnssec-signzone 
			~/bin/dnssec-signzone 
			~/sbin/dnssec-signzone 
			/bin/dnssec-signzone 
			/usr/bin/dnssec-signzone 
			/usr/sbin/dnssec-signzone 
			/usr/local/sbin/dnssec-signzone 
			/opt/sbin/
		     );



my $nosignzone=0;
# try to locate a dnssec-signzone
diag("");
diag("Note: The tests use the file t/random as a source of entropy so that");
diag("      they will not fail with a broken /dev/random");

if ( !( -x "/usr/local/sbin/dnssec-signzone" )){
  diag ("dnssec-signzone not found in the the path specified by default");
  diag ("   /usr/local/sbin/dnssec-signzone");
  diag("I will try other paths");
  diag ("but you will need to set the proper path un the dnssecmaint.conf file");
  diag ("after installation.");
  foreach my $possible_path (@possible_signzone){
      $possible_path=~s/^~/$ENV{"HOME"}/;
    if (-x $possible_path ){
      diag "I will use ".$possible_path;
      $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}=$possible_path. " -r t/random ";
      last;
    }else{
	#diag ("I tried ". $possible_path. " without success");
    }
  }
  $nosignzone=!  $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"};
}else{
  $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}="/usr/local/sbin/dnssec-signzone -r t/random";
}


BEGIN {

    # This makes sence in the original development environment.
    push @INC, "../KeyManager/blib/lib/";
    use_ok('Net::DNS::SEC::Maint::Zone');
    use_ok('Net::DNS::SEC::Maint::ZoneSignerService');
    use_ok('Net::DNS::SEC::Maint::ZoneSigner');
    use_ok ('Net::DNS::SEC::Maint::Key');
}            # test 1-4

my $zone=  Net::DNS::SEC::Maint::Zone->read(0,"t/test.db.2","dacht.net");
ok ( $zone, 'zone read');                         # test 5


my $server = Net::DNS::SEC::Maint::ZoneSignerService->new;


ok ( $server, 'server constructed');                         # test 6

$server->setLocalPort ($testport);
is ($server->getLocalPort,$testport,"set/getLocal Port functionality"); #test 7

$Net::DNS::SEC::Maint::ZoneSigner::PROXY="http://localhost:".$server->getLocalPort;





my $pid;

    $SIG{CHLD}= sub{ 
	if(waitpid($pid,0)>0){
	    my $signalnum= $? & 127;
	    die "child exited on signal $signalnum" if ! WIFEXITED($?) && 
		$signalnum != SIGTERM;
	    
	}
	sleep 1;

	$server->medea;
    

    };

if ($pid=fork){
    # Parent fork.. will kill the server when done.
    sleep(1); # opportunity to get the child start up the server


    eval {
	my $client = Net::DNS::SEC::Maint::ZoneSigner->new;   
	ok ( $client, 'client constructed');                  # test 8    

	my @inct=gmtime(time);
	# One year validity
	my $expirydatestring=  sprintf ("%d%02d%02d%02d%02d%02d",
					 $inct[5]+1901 ,$inct[4]+1 , 
					 $inct[3] ,$inct[2] , $inct[1]  ,
					 $inct[0]);	


	my $startstring=  sprintf ("%d%02d%02d%02d%02d%02d",
					 $inct[5]+1900 ,$inct[4]+1 , 
					 $inct[3] ,$inct[2] , $inct[1]  ,
					 $inct[0]);	


	my $signedzone=$client->signZone(origin=>$zone->get_origin, zone=>$zone->get_zone, start=>$startstring, end=>$expirydatestring);


	$signedzone=~/SIG\s*SOA\s*5\s+2\s+900\s+(\d*)\s+/;

	is( $expirydatestring, $1,"signature on SOA has been generated with argument provided");


	

	#test 9	
    };
    print "Client communication failed: ".$@ if $@;

    kill 15, $pid;
    exit;
}else{
    #Child fork starts up the server and will be killed by the parent.

    die "Cannot fork ".$! unless defined $pid;
    $server->run;
    waitpid($pid,0);

    exit;
}

