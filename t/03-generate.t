#!/usr/bin/perl  -sw
# Test script for Net::DNS::SEC::Maint::Zone
# $Id: 03-generate.t,v 1.10 2005/07/19 10:05:08 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-zonetest.t


use Test::More tests=>5;
use strict;

########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################



BEGIN {
    # This makes sence in the original development environment.
    push @INC, "../KeyManager/blib/lib/";
    use_ok('Net::DNS::SEC::Maint::Zone'); 
}            # test 1

my $nosignzone=0;

$ENV{"DNSSECMAINT_CONFFILE"}="t/dnssecmaint.conf";

my $gid=getgrnam "dnssecmt";
if (!defined $gid){
  print "    dnssecmt is an unknown group\n";
  print "    Please add the group to /etc/group or change the maintgroup \n";
  print "    I will try to use your default group.\n";
  $ENV{"DNSSECMAINT_MAINTGROUP"}=getgrgid($();
}



my @possible_signzone= qw(
			./dnssec-signzone 
			~/dnssec-signzone 
			~/bin/dnssec-signzone 
			~/sbin/dnssec-signzone 
			/bin/dnssec-signzone 
			/usr/bin/dnssec-signzone 
			/usr/sbin/dnssec-signzone 			  
			/usr/local/sbin/dnssec-signzone 
			/opt/sbin/dnssec-signzone 
		     );

diag("");
diag("Note: The tests use the file t/random as a source of entropy so that");
diag("      they will not fail with a broken /dev/random");

if ( !( -x "/usr/local/sbin/dnssec-signzone" )){
  diag ("dnssec-signzone not found in the the path specified by default");
  diag ("   /usr/local/sbin/dnssec-signzone");
  diag("I will try other paths");
  diag ("but you will need to set the proper path un the dnssecmaint.conf file");
  diag ("after installation.");
  foreach my $possible_path (@possible_signzone){
      $possible_path=~s/^~/$ENV{"HOME"}/;
    if (-x $possible_path ){
      diag "I will use ".$possible_path;
      $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}=$possible_path. " -r t/random ";
      last;
    }else{
	#diag ("I tried ". $possible_path. " without success");
    }
  }
  $nosignzone=!  $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"};
}else{
  $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}="/usr/local/sbin/dnssec-signzone -r t/random";
}



my $zone2=  Net::DNS::SEC::Maint::Zone->read(0,"t/test.db.7","dacht.net");
ok ( $zone2, 'direct zone read');                         # test 2

 SKIP: {
     eval { 
	 require Net::DNS::SEC::Maint::Key 
	 };
     skip "Net::DNS::SEC::Maint::Key not installed (OK for systems that do not hold keys)", 2 if $@;
     diag("---Signing zone may take a while---");
     $zone2->signfilename("t/tmp/ZONE-TMP");
     is (  $zone2->sign, 0, 'zone signed');    # test 3
     diag("---Signing zone done---");
     my $systemout=join (" ",$zone2->get_sign_system);
     diag($systemout);
     ok (  $systemout=~/Signatures generated:\s+43/ ,"Proper amount of signatures generated");

     my $namedcheckzone;
     foreach my $possible_path (@possible_signzone){
	 $possible_path=~s/^~/$ENV{"HOME"}/;
	 $possible_path=~s/dnssec-signzone/named-checkzone/;

	 if (-x $possible_path ){


	     open(NAMEDV, $possible_path." -v |") || return (0);
	     my $namedv=<NAMEDV>;
	     $namedv=~ /^(\d+)\.(\d+)\./;
	     close(NAMEDV);
	     (($1>=9) && ($2 >= 3)) || next;

	     diag "I will use ".$possible_path;

	     $namedcheckzone=$possible_path;
	     
	     last;
	 }else{
	     # diag ("I tried ". $possible_path. " without success");
	 }
     }
     if (defined($namedcheckzone)){

	 open(COUNT, $namedcheckzone." -D  dacht.net ". $zone2->signfilename. " |" )|| die "Could not execute command";


	 # Find the start of the actual zone content by stripping the header
       PARSELOAD: while (<COUNT>) {
	   if (/^zone dacht.net\/IN: (.*)/){
	       last PARSELOAD;
	   }
       }
	 my $RRcount=0;
       CONTENT: while (<COUNT>) {
	   # and the zone is nicely cannonicalized by named-checkzone -B
	   last if  /^OK$/;
	   $RRcount++;
       }

	 is ( $RRcount, 91, "Counted correct number of RRs");

     }else{

	 skip "# Skip no named-checkzone found",1;

     }



 
} 



