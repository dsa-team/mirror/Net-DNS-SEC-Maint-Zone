#!/usr/bin/perl  -sw 
# Test script for Net::DNS::SEC::Maint::Zone
# $Id: 01-zonetest.t,v 0.23 2005/07/12 08:15:51 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-zonetest.t

#
#  Use the keydatabase in the test suite.
$ENV{"DNSSECMAINT_CONFFILE"}="t/dnssecmaint.conf";


use Test::More tests=>14;
use strict;
use Data::Dumper;
use File::Temp qw/ :POSIX /;

########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################



my $gid=getgrnam "dnssecmt";
if (!defined $gid){
  print "    dnssecmt is an unknown group\n";
  print "    Please add the group to /etc/group or change the maintgroup \n";
  print "    I will try to use your default group.\n";
  $ENV{"DNSSECMAINT_MAINTGROUP"}=getgrgid($();
}



my @possible_signzone= qw(
			./dnssec-signzone 
			~/dnssec-signzone 
			~/bin/dnssec-signzone 
			~/sbin/dnssec-signzone 
			/bin/dnssec-signzone 
			/usr/bin/dnssec-signzone 
			/usr/sbin/dnssec-signzone 
			/usr/local/sbin/dnssec-signzone 
			/opt/sbin/dnssec-signzone 

		     );



my $nosignzone=0;
# try to locate a dnssec-signzone
diag("");
diag("Note: The tests use the file t/random as a source of entropy so that");
diag("      they will not fail with a broken /dev/random");

if ( !( -x "/usr/local/sbin/dnssec-signzone" )){
  diag ("dnssec-signzone not found in the the path specified by default");
  diag ("   /usr/local/sbin/dnssec-signzone");
  diag("I will try other paths");
  diag ("but you will need to set the proper path un the dnssecmaint.conf file");
  diag ("after installation.");
  foreach my $possible_path (@possible_signzone){
      $possible_path=~s/^~/$ENV{"HOME"}/;
    if (-x $possible_path ){
	diag "I will use ".$possible_path;
	$ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}=$possible_path. " -r t/random ";
	last;
    }else{
	#diag ("I tried ". $possible_path. " without success");
    }
  }
  $nosignzone=!  $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"};
}else{
    $ENV{"DNSSECMAINT_DNSSEC_SIGNZONE"}="/usr/local/sbin/dnssec-signzone -r t/random";
}

BEGIN {
    # This makes sence in the original development environment.
    push @INC, "../KeyManager/blib/lib/";
    use_ok('Net::DNS::SEC::Maint::Zone'); 

}            # test 1




#$Net::DNS::Zone::Parser::NAMED_CHECKZONE=0;



my $zone=  Net::DNS::SEC::Maint::Zone->read(1,"t/test.db.2","dacht.net");

ok ( $zone, 'zone read');                         # test 2




# Write and read data


my $sig_start="20020101120000";
my $sig_end="20020101120020";

$zone->set_sig_start($sig_start);
$zone->set_sig_end($sig_end);


#
#  checking the get_sig* and set_sig* accessor methods
#  tests 6-8

is ($zone->get_sig_start,$sig_start,"Sig_Start options ");  # test 3
is ($zone->get_sig_end,$sig_end,"Sig_End options ");        # test 4






 SKIP: {
     
     eval { 
	 require Net::DNS::SEC::Maint::Key 
	 };
     skip "Net::DNS::SEC::Maint::Key not installed (OK for systems that do not hold keys)", 3 if $@;
     diag("---Signing zone may take a while---");
     $zone->signfilename("t/tmp/ZONE-TMP");
     is (  $zone->sign, 0, 'zone signed');    # test 5
     diag("---Signing zone done---");


     my $zone2=  Net::DNS::SEC::Maint::Zone->read(1,"t/test.db.4","notindb.example");
     ok ( $zone2, 'zone read');           #test 6

     is ( $zone2->sign, "There are no active zone keys for notindb.example.\n","Not able to sign a zone without a keys in the DB");  # test 7


 } 













SKIP: {
     eval { require Net::DNS::SEC::Maint::Key };
     skip "Net::DNS::SEC::Maint::Key not installed (OK for systems that do not hold keys)", 6 if $@;
     my $zone5=  Net::DNS::SEC::Maint::Zone->read(1,"t/test.db.3","foo.example.");
     ok ( $zone5, 'zone read');           #test 8
     $zone5->signfilename("t/tmp.signed");
     $zone5->set_sig_end("now+300");



     is ( $zone5->sign, "0","signed DB");  # test 9

     is ($zone5->signfilename, "t/tmp.signed" , "signfilename set");
                                           #test 10

     my @errors=$zone5->get_sign_errors;
     my $everythingfine=1;
     my $signedzone=$zone5->get_signedzone;
#     print $signedzone;

     print join (" ",@errors);
     foreach my $error (@errors){
	 $everythingfine=0 if $error !~ /^dnssec-signzone: warning.*signature has expired\s*$/;
     }
     ok ( $everythingfine, "No unexpected error output");
                                            #test 11
     #
     # Even more elaborate tests:
     my $systemout=join (" ",$zone5->get_sign_system);

     diag($systemout);

     ok (  $systemout=~/Signatures generated:\s+211/ ,"Proper amount of signatures generated");
                                            # test 12

     ok (  $systemout=~/Signatures dropped:\s+208/ ,"Proper amount of signatures dropped"); # test 13
     



     # leftover generated by the signer.
     unlink("keyset-foo.example.");
     unlink("keyset-t/tmp.signed.");

 }




my $zone5=  Net::DNS::SEC::Maint::Zone->read(1,"t/dacht.net","   ");
ok ( $zone5, 'zone read');                         # test 14





