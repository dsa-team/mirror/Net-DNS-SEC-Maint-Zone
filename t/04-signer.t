# $Id: 04-signer.t,v 1.3 2005/07/06 19:26:20 olaf Exp $  -*-perl-*-

use Test::More;
use strict;

use Test::More tests => 7;



########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################



BEGIN { 
    # This makes sence in the original development environment.
    push @INC, "../KeyManager/blib/lib/";
    use_ok( 'Net::DNS::SEC::Maint::ZoneSigner' ); 
}

my $pid;
unless ($pid = fork()) {
  require Net::DNS::SEC::Maint::ZoneSignerService;
  my $s = Net::DNS::SEC::Maint::ZoneSignerService->new;
  $s->run();
}

sleep 2;

ok($pid, "ZoneSignerService pid: $pid");

my $z = Net::DNS::SEC::Maint::ZoneSigner->new;

my $r = rand();
ok($z->testEcho($r) eq $r, 'testEcho');

my $i;
ok($z->testPersist() eq ++$i, "testPersist $i") for (1..3);

eval { $z->testError('error!error!error') };
ok($@ =~ /^error!error!error/, 'Exceptions');

kill 9, $pid;

wait;



