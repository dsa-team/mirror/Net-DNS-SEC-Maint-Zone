#!/usr/bin/perl  -sw
# Test script for Net::DNS::SEC::Maint::Zone
# $Id: 02-include.t,v 1.6 2005/07/06 19:26:20 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-zonetest.t


#  Use the keydatabase in the test suite.
$ENV{"DNSSECMAINT_CONFFILE"}="t/dnssecmaint.conf";


use Test::More tests=>5;
use strict;

########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################




BEGIN {    
    # This makes sence in the original development environment.
    push @INC, "../KeyManager/blib/lib/";
    use_ok('Net::DNS::SEC::Maint::Zone'); 
}            # test 1

my $zone2=  Net::DNS::SEC::Maint::Zone->read(0,"t/test.db.2","dacht.net");
ok ( $zone2, 'direct zone read');                         # test 2


my $zone=  Net::DNS::SEC::Maint::Zone->read(0,"t/test.db.5","dacht.net");
ok ( $zone, 'included zone read');                         # test 3


is ( $zone->get_zone,$zone2->get_zone,"Included and direct read equal");
                                                           # test 4





my $zone3=  Net::DNS::SEC::Maint::Zone->read(0,"t/test.db.8","example.com");

ok ( $zone3, ' zone read');                         # test 5

