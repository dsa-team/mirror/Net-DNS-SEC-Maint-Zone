package Net::DNS::SEC::Maint::ZoneSigner;

use strict;
use SOAP::Lite;

our $VERSION = '0.00_01';

our $URI = 'http://www.ripe.net/Net/DNS/SEC/Maint/Zone/Wrapper';
our $PROXY = 'http://localhost:8001/';
our $ON_FAULT = sub
  { my($soap, $res) = @_;
    die  ref $res ? $res->faultstring: $soap->transport->status, "\n";
  };

sub new {
  my $class = shift;

  my $soap = SOAP::Lite->new;
  $soap->uri($URI);
  $soap->proxy($PROXY);
  $soap->on_fault($ON_FAULT);

  return bless {soap=>$soap, obj=>$soap->call('new')->result }, $class;
}

sub AUTOLOAD {
  my ($method) = our $AUTOLOAD =~ /::([^:]+)$/;

  no strict 'refs';
  *$AUTOLOAD = eval <<EOF_SUB;
  sub{
    my \$self = shift;
    \$self->{soap}->call('$method'=>\$self->{obj},\@_)->result;
  }
EOF_SUB

  goto &$AUTOLOAD;
}
 
sub DESTROY {
  my $self = shift;
  $self->{soap}->call('DESTROY'=>$self->{obj},@_)->result;
}


1;

__END__

=head1 NAME

Net::DNS::SEC::Maint::ZoneSigner - Zone Signer Client Module

=head1 SYNOPSIS

  use Net::DNS::SEC::Maint::ZoneSigner;

  $z = Net::DNS::SEC::Maint::ZoneSigner->new;

  $signed_zone = $z->signZone(origin=>'ori.gin.', zone=>$zone);

=head1 DESCRIPTION

This Perl module is a client to
Net::DNS::SEC::Maint::ZoneSignerService which in turn a wrapper for
Net::DNS::SEC::Maint::Zone to make it avaliable over network
connections.

=head1 METHODS

=over 1

=item new ( )

Constructor.

=item signZone ( origin=>ORIGIN, zone=>ZONE, start=>START, end=>END )

Signes a given zone with matching keys in DNSSEC key database. ORIGIN
is the origin of the zone file. ZONE is content of the zone
file. Returns a scalar with the content of the signed zone file. START
and END are dates.

=back


=head1 CLASS VARIABLES

=over 1

=item $Net::DNS::SEC::Maint::ZoneSigner::PROXY

By default it is http://localhost:8001/. You should set this variable
(before calling 'new') to match
Net::DNS::SEC::Maint::ZoneSignerService connection parameters.

=back

=head1 AUTHOR

Ziya Suzen E<lt>ziya@ripe.netE<gt>

=head1 SEE ALSO

perl(1), Net::DNS::SEC::Maint::ZoneSignerService(3),
Net::DNS::SEC::Maint::Zone(3).

=head1 COPYRIGHT

Copyright (c) 2003                                          RIPE NCC 

All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the author not be
used in advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS; IN NO EVENT SHALL
AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

