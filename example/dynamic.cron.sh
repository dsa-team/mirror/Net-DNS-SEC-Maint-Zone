#!/bin/sh


# (To) simple example script for resigning a dynamic zone.
#  All variables are hardcoded in the script.
#
#  This is a example script only. You should extend the error checks.
#
# O.M. Kolkman
# RIPE NCC 2002
# Copyright notice below.
# $Id: dynamic.cron.sh,v 1.2 2002/11/11 15:33:07 olaf Exp $

#
# USE WITH CARE. THE SCRIPT DOES NOT CONTAIN ANY ERROR CHECKS AND WILL OVERWRITE
# YOUR ORIGINAL ZONE FILE.  DO NOT USE THIS SCRIPT ON PRODUCTION ZONES WITHOUT
# MODIFICATIONS
#


#######################
#
# Make sure the proper configuration file is used.
DNSSECMAINT_CONFFILE=/usr/local/etc/dnssecmaint.conf


#######################
# origin of the zone
# 
DYNZONE_ORIGIN=example.foo.

#######################
# Directory where named looks for keys when performing dynamic updates
#
DYNZONE_KEYDIR=/usr/local/var/named/

#######################
# Directory from which the zone files are loaded
# 
DYNZONE_DIR=/usr/local/var/named/zones/




#######################
# Location of the signed zonefile loaded by named.
# The signer uses the signed file, it willl be resigned.
#
# Note: dnssec-signzone will only resign signatures that are in need 
# of resignin.

DYNZONE_ZONEFILE=${DYNZONE_DIR}/example.foo.signed



/usr/local/sbin/rndc freeze ${DYNZONE_ORIGIN}
/usr/local/bin/dnssecmaint-copyprivate ${DYNZONE_ORIGIN} ${DYNZONE_DIR}
/usr/local/bin/dnssigner -o ${DYNZONE_ORIGIN} ${DYNZONE_ZONEFILE}


################
#
# You should include all sorts of heuristic checks... Direct copying
# may be damaging.
# 


cp  ${DYNZONE_ZONEFILE}.signed ${DYNZONE_ZONEFILE} 
/usr/local/sbin/rndc unfreeze ${DYNZONE_ORIGIN}







#
# Standard disclaimer and copyright.
#
#    Copyright (c) 2002  RIPE NCC.  Author Olaf M. Kolkman
#
#    All Rights Reserved
#
#    Permission to use, copy, modify, and distribute this software and its
#    documentation for any purpose and without fee is hereby granted,
#    provided that the above copyright notice appear in all copies and that
#    both that copyright notice and this permission notice appear in
#    supporting documentation, and that the name of the author not be
#    used in advertising or publicity pertaining to distribution of the
#    software without specific, written prior permission.
#
#
#    THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
#    ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS; IN NO EVENT SHALL
#    AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
#    DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
#    AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.







